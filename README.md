# README #

This project demonstrates the usage of the the bitbucket pipe for [ComplianceDB](https://www.compliancedb.com/).

You can see the full running pipeline in [bitbucket-pipelines.yml](bitbucket-pipelines.yml)

Learn more about `cdb_controls` here: https://github.com/compliancedb/cdb_controls 
