image: python:3.7.3
options:
  docker: true

pipelines:
  branches:
    production:
      - step:
          script:
            - echo Do nothing on production

  default:
    - step:
        name: Establish pipeline in ComplianceDB
        script:
          - pipe: docker://compliancedb/cdb_controls-bbpipe:latest
            variables:
              CDB_PIPELINE_DEFINITION: pipeline-file-artifact.json
              CDB_COMMAND: 'put_pipeline'
              CDB_API_TOKEN: $CDB_API_TOKEN
    - step:
        name: Build
        script:
          - set
          - date > artifact.txt  # Simulate a non-deterministic build
        artifacts:
          - "artifact.txt"

    - step:
        name: Publish artifact in ComplianceDB
        script:
          - ls -al
          - pipe: docker://compliancedb/cdb_controls-bbpipe:latest
            variables:
              CDB_PIPELINE_DEFINITION: pipeline-file-artifact.json
              CDB_COMMAND: 'put_artifact'
              CDB_API_TOKEN: $CDB_API_TOKEN
              CDB_IS_COMPLIANT: "TRUE"
              CDB_ARTIFACT_FILENAME: "artifact.txt"


    - step:
        name: Control junit results and publish to ComplianceDB
        script:
          - mkdir -p /data/junit
          - cp test_results.xml /data/junit
          - pipe: docker://compliancedb/cdb_controls-bbpipe:latest
            variables:
              CDB_PIPELINE_DEFINITION: pipeline-file-artifact.json
              CDB_COMMAND: 'control_junit'
              CDB_API_TOKEN: $CDB_API_TOKEN
              CDB_TEST_RESULTS_DIR: "/data/junit"
              CDB_EVIDENCE_TYPE: "pytest_test_results"
              CDB_ARTIFACT_FILENAME: "artifact.txt"
              CDB_DESCRIPTION: "Tests passed"

    - step:
        name: Control Bitbucket Pull Request and publish result to ComplianceDB
        script:
          - pipe: docker://compliancedb/cdb_controls-bbpipe:latest
            variables:
              CDB_PIPELINE_DEFINITION: pipeline-file-artifact.json
              CDB_COMMAND: 'control_bitbucket_pr'
              CDB_API_TOKEN: $CDB_API_TOKEN
              CDB_ARTIFACT_FILENAME: "artifact.txt"
              # These should be provided as repository variables from an app password
              BITBUCKET_API_USER: $BITBUCKET_API_USER
              BITBUCKET_API_TOKEN: $BITBUCKET_API_TOKEN
              CDB_FAIL_PIPELINE: "FALSE"
              CDB_FORCE_COMPLIANT: "TRUE"


    # Docker pipeline steps
    - step:
        name: Establish docker pipeline in ComplianceDB
        script:
          - pipe: docker://compliancedb/cdb_controls-bbpipe:latest
            variables:
              CDB_PIPELINE_DEFINITION: pipeline-docker-artifact.json
              CDB_COMMAND: 'put_pipeline'
              CDB_API_TOKEN: $CDB_API_TOKEN

    - step:
        name: Build and publish docker image
        script:
          - docker build -t compliancedb/hello .
          - docker login --username $DOCKER_USERNAME --password $DOCKER_PASSWORD
          - docker push compliancedb/hello
          - pipe: docker://compliancedb/cdb_controls-bbpipe:latest
            variables:
              CDB_PIPELINE_DEFINITION: pipeline-docker-artifact.json
              CDB_COMMAND: 'put_artifact_image'
              CDB_API_TOKEN: $CDB_API_TOKEN
              CDB_ARTIFACT_DOCKER_IMAGE: compliancedb/hello
              CDB_IS_COMPLIANT: "TRUE"

    - step:
        name: Test docker image
        script:
          - docker container run compliancedb/hello # simulate test run
          - mkdir -p /data/junit
          - cp test_results.xml /data/junit
          # Publish to ComplianceDB
          - pipe: docker://compliancedb/cdb_controls-bbpipe:latest
            variables:
              CDB_PIPELINE_DEFINITION: pipeline-docker-artifact.json
              CDB_COMMAND: 'control_junit'
              CDB_API_TOKEN: $CDB_API_TOKEN
              CDB_ARTIFACT_DOCKER_IMAGE: compliancedb/hello
              CDB_TEST_RESULTS_DIR: "/data/junit"
              CDB_EVIDENCE_TYPE: "pytest_test_results"
              CDB_DESCRIPTION: "Tests passed"

    - step:
        name: Control Bitbucket Pull Request and publish result to ComplianceDB
        script:
          - docker image pull compliancedb/hello
          - pipe: docker://compliancedb/cdb_controls-bbpipe:latest
            variables:
              CDB_PIPELINE_DEFINITION: pipeline-docker-artifact.json
              CDB_COMMAND: 'control_bitbucket_pr'
              CDB_API_TOKEN: $CDB_API_TOKEN
              CDB_ARTIFACT_DOCKER_IMAGE: compliancedb/hello
              # These should be provided as repository variables from an app password
              BITBUCKET_API_USER: $BITBUCKET_API_USER
              BITBUCKET_API_TOKEN: $BITBUCKET_API_TOKEN
              CDB_FAIL_PIPELINE: "FALSE"
              CDB_FORCE_COMPLIANT: "TRUE"

    - step:
        name: Make release for file artifact in ComplianceDB
        trigger: manual
        script:
          # Need to fetch all the branches since bitbucket does shallow clones
          - git fetch origin "+refs/heads/*:refs/remotes/origin/*"
          - git branch -av
          - pipe: docker://compliancedb/cdb_controls-bbpipe:latest
            variables:
              CDB_PIPELINE_DEFINITION: pipeline-file-artifact.json
              CDB_COMMAND: 'create_release'
              CDB_API_TOKEN: $CDB_API_TOKEN
              CDB_ARTIFACT_FILENAME: "artifact.txt"
              CDB_RELEASE_DESCRIPTION: "Release created in pipeline"
              CDB_TARGET_SRC_COMMITISH: master
              CDB_BASE_SRC_COMMITISH: origin/production

    - step:
        name: Simulate file deployment and log in compliancedb
        script:
          - echo Fake Deployment Command Klaxon
          - pipe: docker://compliancedb/cdb_controls-bbpipe:latest
            variables:
              CDB_PIPELINE_DEFINITION: pipeline-file-artifact.json
              CDB_COMMAND: 'create_deployment'
              CDB_API_TOKEN: $CDB_API_TOKEN
              CDB_ARTIFACT_FILENAME: "artifact.txt"
              CDB_DESCRIPTION: "Deployed to production from pipeline"
              CDB_ENVIRONMENT: production
              CDB_USER_DATA: user_data.json


    - step:
        name: Make release for docker image in ComplianceDB
        trigger: manual
        script:
          # Need to fetch all the branches since bitbucket does shallow clones
          - docker image pull compliancedb/hello
          - git fetch origin "+refs/heads/*:refs/remotes/origin/*"
          - git branch -av
          - pipe: docker://compliancedb/cdb_controls-bbpipe:latest
            variables:
              CDB_PIPELINE_DEFINITION: pipeline-docker-artifact.json
              CDB_COMMAND: 'create_release'
              CDB_API_TOKEN: $CDB_API_TOKEN
              CDB_ARTIFACT_DOCKER_IMAGE: compliancedb/hello
              CDB_RELEASE_DESCRIPTION: "Release created in pipeline"
              CDB_TARGET_SRC_COMMITISH: master
              CDB_BASE_SRC_COMMITISH: origin/production

    - step:
        name: Simulate docker deployment and log in compliancedb
        script:
          - echo Fake Deployment Command Klaxon
          - pipe: docker://compliancedb/cdb_controls-bbpipe:latest
            variables:
              CDB_PIPELINE_DEFINITION: pipeline-docker-artifact.json
              CDB_COMMAND: 'create_deployment'
              CDB_API_TOKEN: $CDB_API_TOKEN
              CDB_ARTIFACT_DOCKER_IMAGE: compliancedb/hello
              CDB_DESCRIPTION: "Deployed to production from pipeline"
              CDB_ENVIRONMENT: production
              CDB_USER_DATA: user_data.json